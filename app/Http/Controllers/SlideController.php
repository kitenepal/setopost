<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Auth;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        $slide=new Slide();
        $slide->user_id=Auth::id();
        $slide->slide_date=date('Y-m-d');
        $slide->pid=$id;
        $slide->status=0;
        $slide->active=0;

        $slide->save();
        return redirect('slide');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Slide::find($id);

        $data->delete();
        return redirect('slide');
    }

    public function inactivate(Request $request, $id)
    {
        //
        $slide= Slide::find($id);
        $slide->user_id=Auth::id();
        $slide->status=1;
        $slide->save();
        return redirect('slide');
    }

    public function activate(Request $request, $id)
    {
        //
        $slide= Slide::find($id);
        $slide->user_id=Auth::id();
        $slide->status=0;
        $slide->save();
        return redirect('slide');
    }
}
