<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;
use App\Group;
use App\Menu;
use App\Setting;
use App\Post;
use App\Addvert;
use App\Title;
use App\Slide;
use DB;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        return view('back.admin');
    }
   public function error()
    {
        return view('back.pages.error');
    }

    // public function addPost()
    // {
    //     return view('back.pages.addPost');
    // }

     public function group()
    {
        $group=Group::all();
       // dd($group);
        return view('back.group.create',['rows'=>$group]);
    }

     public function menu()
    {
        $menu=Menu::all();
       //dd($menu);
        return view('back.menu.create',['rows'=>$menu]);
    }

    // public function setting()
    // {
    //    $setting=Setting::find(1);
    //    //dd($setting);
    //    return view('back.setting.edit',['rows' => $setting]);
    // }

    public function post()
    {
        //$post=Post::all();
        $post = DB::table('posts')
            ->join('groups', 'posts.gid', '=', 'groups.id')
            ->join('menus', 'posts.mid', '=', 'menus.id')
            ->select('posts.*', 'groups.group', 'menus.menu')
            ->get();
        //$group=Group::find(1);
        //return $post;
        //dd($post);
        return view('back.post.index',['rows'=>$post],compact('post'));

    }

    public function addpost()
    {
        $group=Group::all();
        $menu=Menu::all();
        return view('back.post.create',['g_rows'=>$group],['m_rows'=>$menu]);
    }

    public function addvert()
    {
        $addvert = DB::table('addverts')
            ->join('groups', 'addverts.gid', '=', 'groups.id')
            ->join('menus', 'addverts.mid', '=', 'menus.id')
            ->select('addverts.*', 'groups.group', 'menus.menu')->get();
        //dd($addvert);
        $group=Group::all();
        $menu=Menu::all();
       //dd($addvert);
        return view('back.addvert.create',
            ['rows'=>$addvert,
            'g_rows'=>$group,
            'm_rows'=>$menu]);
    }

    public function title()
    {
        $post = Post::all();

        // $post=DB::table('posts')->select('posts.*')->whereNotExists('posts.id','!=','titles.pid')->get();

       // dd($post);

        $title=DB::table('titles')
            ->join('posts', 'titles.pid', '=', 'posts.id')
            ->select('titles.*', 'posts.title')->get();

        return view('back.title.index',['p_rows'=>$post,'t_rows'=>$title]);

    }


    public function slide()
    {
        $post = Post::all();

        $slide=DB::table('slides')
            ->join('posts', 'slides.PID', '=', 'posts.id')
            ->select('slides.*', 'posts.title')->get();

        return view('back.slide.index',['p_rows'=>$post,'s_rows'=>$slide]);
    }

    public function user()
    {
        return view('back.pages.user');
    }  

    
 
}
