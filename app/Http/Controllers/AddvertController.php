<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addvert;
use Auth;
use File;
use DB;
use App\Group;
use App\Menu;

class AddvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $addvert= new Addvert();
        $addvert->user_id=Auth::id();
       
       //image upload and save
        $files=$request->image;
        //dd($files);
        if($files){
            $dest_path = "img/addvert/";
            $filename =uniqid()."-".$files->getClientOriginalName();
            // if(!is_null($data->image)){
            //     $file_path= $data->image;
            //     if(file_exists($file_path))
            //         unlink($file_path);
            // }
            $files->move($dest_path,$filename);
            $image = $dest_path.$filename;
            //$image = $filename;
            $addvert->image=$image;
        }
       
        // $header=$request->header;
        // if(isset($header))
        // {
        //     $h=1;
        // }
        // else
        // {
        //     $h=0;
        // }
        $addvert->header=0;
        $addvert->mid=$request->page;
        $addvert->gid=$request->section;
        $addvert->type=$request->type;
        $addvert->from=$request->from;
        $addvert->till=$request->till;
        $addvert->meta=$request->meta;
        $addvert->caption=$request->caption;
        $addvert->links=$request->links;
        $addvert->keywords=$request->keywords;
        $addvert->metaDesc=$request->metaDesc;
        $addvert->status=0;
        $addvert->index=0;

        //dd($addvert);
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $addvert->save();
        return redirect('addvert');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $addvert = DB::table('addverts')
            ->join('groups', 'addverts.gid', '=', 'groups.id')
            ->join('menus', 'addverts.mid', '=', 'menus.id')
            ->select('addverts.*', 'groups.group', 'menus.menu')->get();
        //dd($addvert);
        $group=Group::all();
        $menu=Menu::all();
        $result=Addvert::find($id);
        $header=DB::table('addverts')
            ->join('groups', 'addverts.gid', '=', 'groups.id')
            ->join('menus', 'addverts.mid', '=', 'menus.id')
            ->select('addverts.*', 'groups.group', 'menus.menu')->where('addverts.id','=',$id)->get();
       //dd($result);

        return view('back.addvert.edit',
            ['rows'=>$addvert,
            'result'=>$result,
            'g_rows'=>$group,
            'm_rows'=>$menu,
            'h_rows'=>$header]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $addvert=Addvert::find($id);
        //dd($group);
        $addvert->user_id=Auth::id();

        $files=$request->image;

         if(!is_null($files))
        {
            $file_path= $request->pic;
            if(file_exists($file_path))
            {
            unlink($file_path);    
            $dest_path = "img/addvert/";
            $filename =uniqid()."-".$files->getClientOriginalName();
            // if(!is_null($data->image)){
            //     $file_path= $data->image;
            //     if(file_exists($file_path))
            //         unlink($file_path);
            // }
            $files->move($dest_path,$filename);
            $image = $dest_path.$filename;
            //$image = $filename;
            $addvert->image=$image;
            }
           
        }
        else
        {
            $addvert->image=$request->pic;  
            //dd($post); 
        } 
        // $header=$request->header;
        // if(isset($header))
        // {
        //     $h=1;
        // }
        // else
        // {
        //     $h=0;
        // }
        // $addvert->header=$h;
        $addvert->mid=$request->page;
        $addvert->gid=$request->section;
        $addvert->type=$request->type;
        $addvert->from=$request->from;
        $addvert->till=$request->till;
        $addvert->meta=$request->meta;
        $addvert->caption=$request->caption;
        $addvert->links=$request->links;
        $addvert->keywords=$request->keywords;
        $addvert->metaDesc=$request->metaDesc;
        
        $addvert->save();
        return redirect('addvert');
    }


    public function header($id)
    {
        //
        $header=Addvert::find($id);
        $addvert=DB::table('addverts')->select('*')->where([['header','=',1],['status','=',1]])->get();
       //dd($header,$addvert);
        if($addvert->isEmpty())
        {    
            $header->header=1;
        }
        else
        {
            foreach($addvert as $data)
            {
                
                //dd($data);
                
                    //dd($id);
                    $d=$data->id;
                    $t=$header->id;
                   // dd($d,$t);
                    if($d==$t)
                    {
                        $header->header=0; 
                    }
                    else
                    {
                        $header->header=1;
                    }
                
            }
        }

        //dd($addvert->header);
        $header->user_id=Auth::id();
        //$header=$request->header;
        
        $header->save();
        return redirect('addvert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Addvert::find($id);

        if(!is_null($data->image)){
            $file_path= $data->image;
            if(file_exists($file_path))
                unlink($file_path);
        }

        $data->delete();
        return redirect('addvert');
    }

    public function inactivate(Request $request, $id)
    {
        //
        $addvert= Addvert::find($id);
        $addvert->user_id=Auth::id();
        $addvert->status=1;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        //dd($post);
        $addvert->save();
        return redirect('addvert');
    }

    public function activate(Request $request, $id)
    {
        //
        $addvert= Addvert::find($id);
        $addvert->user_id=Auth::id();
        $addvert->status=0;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $addvert->save();
        return redirect('addvert');
    }
}
