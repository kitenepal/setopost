<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Auth;
use Input as Input;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting=Setting::all();
        $result=Setting::find($id);
       //dd($result);

        return view('back.setting.edit',['rows'=>$setting,'result'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $setting=Setting::find($id);
        //dd($setting);
        $setting->user_id=Auth::id();
        $setting->title=$request->title;
        $setting->address=$request->address;
        $setting->contact=$request->contact;
        $setting->email=$request->email;
        $setting->image=$request->image;
        $setting->google_map=$request->google_map;
        $setting->facebook=$request->facebook;
        $setting->twitter=$request->twitter;
        $setting->google_plus=$request->google_plus;
        $setting->pinterest=$request->pininterest;
        $setting->linkedin=$request->linkedin;
        $setting->youtube=$request->youtube;
        $setting->desc=$request->description;
        $setting->meta=$request->meta;
        $setting->caption=$request->caption;
        $setting->links=$request->links;
        $setting->keywords=$request->keywords;
        $setting->metadesc=$request->metaDesc;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        if(Input::hasFile('file')){
            $file=Input::file('file');
            $file->move('uploads',$file->getClientOriginalName());
            // echo '<img src="uploads/'.$file->getClientOriginalName().'"/>';
            $file=$file->getClientOriginalName();
            $setting->image=$request->$file;
        }
        dd($setting);

        //$setting->save();
        return redirect('setting\edit\1');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
