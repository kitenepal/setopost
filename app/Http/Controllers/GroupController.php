<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $group= new Group();
        $group->user_id=Auth::id();
        $group->group=$request->group;
        $group->meta=$request->meta;
        $group->caption=$request->caption;
        $group->links=$request->links;
        $group->keywords=$request->keywords;
        $group->metaDesc=$request->metaDesc;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $group->save();
        return redirect('group');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $group=Group::all();
       $result=Group::find($id);
       //dd($result);

        return view('back.group.edit',['rows'=>$group,'result'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $group=Group::find($id);
        //dd($group);
        $group->user_id=Auth::id();
        $group->group=$request->group;
        $group->meta=$request->meta;
        $group->caption=$request->caption;
        $group->links=$request->links;
        $group->keywords=$request->keywords;
        $group->metaDesc=$request->metaDesc;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $group->save();
        return redirect('group');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //dd($id);
        $data=Group::find($id);

        $data->delete();
        return redirect('group');
    }
}
