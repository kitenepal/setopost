<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use DB;
use App\Group;
use App\Menu;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $post= new Post();
        $post->user_id=Auth::id();
        $post->title=$request->title;
        $post->description=$request->description;
        $post->gid=$request->group;
        $post->mid=$request->menu;
        $post->post_by=Auth::id();
        $post->meta=$request->meta;
        $post->caption=$request->caption;
        $post->links=$request->links;
        $post->keywords=$request->keywords;
        $files=$request->image;
        //dd($files);
        if($files){
            $dest_path = "img/post/";
            $filename =uniqid()."-".$files->getClientOriginalName();
            // if(!is_null($data->image)){
            //     $file_path= $data->image;
            //     if(file_exists($file_path))
            //         unlink($file_path);
            // }
            $files->move($dest_path,$filename);
            $image = $dest_path.$filename;
            //$image = $filename;
            $post->feature_img=$image;
        }
        $post->status=0;
        $date=$request->date;

        if($date=="")
        {
            $post->created_at=today();
        }
        else
        {
            $post->created_at=$date;
            
        }
        $post->save();
        return redirect('post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post=Post::all()->where('id','=',$id);
        return view('back.post.view',['rows'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $post = DB::table('posts')
            ->join('groups', 'posts.gid', '=', 'groups.id')
            ->join('menus', 'posts.mid', '=', 'menus.id')
            ->select('posts.*', 'groups.group', 'menus.menu')->where('posts.id','=',$id)->get();
        $group=Group::all();
        $menu=Menu::all();
        //$group=Group::find(1);
        //return $post;
        //dd($post);
        return view('back.post.edit',['rows'=>$post,'grows'=>$group,'mrows'=>$menu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post= Post::find($id);
        $post->user_id=Auth::id();
        $post->title=$request->title;
        $post->description=$request->description;
        //$post->feature_img=$request->file;
        $post->gid=$request->group;
        $post->mid=$request->menu;
        $post->post_by=Auth::id();
        $post->meta=$request->meta;
        $post->caption=$request->caption;
        $post->links=$request->links;
        $post->keywords=$request->keywords;
        //$post->feature_img=$request->image;
        $files=$request->image;
        //dd($files);
        if(!is_null($files))
        {
            $file_path= $request->pic;
            if(file_exists($file_path))
            {
            unlink($file_path);    
            $dest_path = "img/post/";
            $filename =uniqid()."-".$files->getClientOriginalName();
            // if(!is_null($data->image)){
            //     $file_path= $data->image;
            //     if(file_exists($file_path))
            //         unlink($file_path);
            // }
            $files->move($dest_path,$filename);
            $image = $dest_path.$filename;
            //$image = $filename;
            $post->feature_img=$image;
            }
           
        }
        else
        {
            $post->feature_img=$request->pic;  
            //dd($post); 
        } 
        $date=$request->date;

        if($date=="")
        {
            $post->created_at=today();
        }
        else
        {
            $post->created_at=$date;
            
        }
        $post->save();
        return redirect('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Post::find($id);
        if(!is_null($data->feature_img)){
            $file_path= $data->feature_img;
            if(file_exists($file_path))
                unlink($file_path);
        }
        $data->delete();
        return redirect('post');
    }

    public function inactivate(Request $request, $id)
    {
        //
        $post= Post::find($id);
        $post->user_id=Auth::id();
        $post->status=1;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        //dd($post);
        $post->save();
        return redirect('post');
    }

    public function activate(Request $request, $id)
    {
        //
        $post= Post::find($id);
        $post->user_id=Auth::id();
        $post->status=0;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $post->save();
        return redirect('post');
    }

    // public function imageUpload($files){
    //     if($files) {
    //         if (!empty($files)) {

    //             $dest_path = "img/post/";
    //             $filename = uniqid() . "-" . $files->getClientOriginalName();
    //             $files->move($dest_path, $filename);
    //             $image = $dest_path . $filename;

    //         }

    //         return $image;
    //     }
    //     return false;
    // }
}
