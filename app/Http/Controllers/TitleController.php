<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Title;
use Auth;

class TitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        $title=new Title();
        $title->user_id=Auth::id();
        $title->title_date=date('Y-m-d');
        $title->pid=$id;
        $title->status=0;

        $title->save();
        return redirect('title');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Title::find($id);

        $data->delete();
        return redirect('title');
    }

    public function inactivate(Request $request, $id)
    {
        //
        $title= Title::find($id);
        $title->user_id=Auth::id();
        $title->status=1;
        $title->save();
        return redirect('title');
    }

    public function activate(Request $request, $id)
    {
        //
        $title= Title::find($id);
        $title->user_id=Auth::id();
        $title->status=0;
        $title->save();
        return redirect('title');
    }
}
