<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Fun;
use auth;
use DB;
use App\Setting;
use App\Menu;
use App\Title;
use App\Group;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $setting=Setting::all();
        $menu=DB::table('menus')->select('*')->orderBy('index', 'asc')->get();
        $title=DB::table('posts')
            ->join('titles', 'posts.id', '=', 'titles.pid')->select('posts.*')->where('titles.status','=',1)->get();
        $slide=DB::table('posts')
            ->join('slides', 'posts.id', '=', 'slides.pid')->select('posts.*','slides.active')->where('slides.status','=',1)->get();
        $r_rows=DB::table('posts')->select('*')->where('status','=',1)->orderBy('created_at','desc')->limit(20)->get();
        $recent=DB::table('posts')->select('*')->where('status','=',1)->orderBy('created_at','desc')->limit(20)->get();
        // $group=DB::table('posts')
        //     ->join('groups', 'posts.gid', '=', 'groups.id')->select('posts.*','groups.group')->where('posts.status','=',1)->get();
        $group=DB::table('posts')->join('groups', 'posts.gid', '=', 'groups.id')->select('group','gid')->orderBy('groups.index', 'asc')->distinct()->get(['gid']);
        $add=DB::table('addverts')
        ->join('menus', 'addverts.mid', '=', 'menus.id')
        ->join('groups', 'addverts.gid', '=', 'groups.id')
        ->select('addverts.*')->where([['addverts.header','=',1],['addverts.status','=',1]])->orderBy('addverts.index', 'asc')->get();
        //dd($add);
        return view('front.home',['s_rows'=>$setting,'m_rows'=>$menu,'sl_rows'=>$slide,'r_rows'=>$r_rows,'recent'=>$recent,'t_rows'=>$title,'g_rows'=>$group,'a_rows'=>$add]);
    }

   public function pages($id)
    {
        $setting=Setting::all();
        $menu=DB::table('menus')->select('*')->orderBy('index', 'asc')->get();
        $mn=DB::table('menus')->select('menus.*')->where('menus.id','=',$id)->get();
        $page=DB::table('posts')->join('menus', 'posts.mid', '=', 'menus.id')->select('posts.*')->where([['posts.mid','=',$id],['posts.status','=',1]])->orderBy('posts.created_at', 'desc')->limit(10)->get();
        $recent=DB::table('posts')->select('*')->where('status','=',1)->orderBy('created_at','desc')->limit(20)->get();
        $add=DB::table('addverts')
        ->join('menus', 'addverts.mid', '=', 'menus.id')
        ->join('groups', 'addverts.gid', '=', 'groups.id')
        ->select('addverts.*')->where([['addverts.header','=',1],['addverts.status','=',1]])->orderBy('addverts.index', 'asc')->get();
        //dd($page);
        return view('front.pages.common',['s_rows'=>$setting,'m_rows'=>$menu,'mn_rows'=>$mn,'p_rows'=>$page,'a_rows'=>$add,'recent'=>$recent]);
    }
    

    public function detail()
    {
        return view('front.pages.detail');
    }

    public function login()
    {
        return view('auth.login');
    }   

    public function logout()
    {
        auth::logout();
        return view('auth.login');
    }   

}
