<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Auth;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $menu= new Menu();
        $menu->user_id=Auth::id();
        $menu->menu=$request->menu;
        $menu->meta=$request->meta;
        $menu->caption=$request->caption;
        $menu->links=$request->links;
        $menu->keywords=$request->keywords;
        $menu->metaDesc=$request->metaDesc;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $menu->save();
        return redirect('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $menu=Menu::all();
        $result=Menu::find($id);
       //dd($result);

        return view('back.menu.edit',['rows'=>$menu,'result'=>$result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $menu=Menu::find($id);
        //dd($group);
        $menu->user_id=Auth::id();
        $menu->menu=$request->menu;
        $menu->meta=$request->meta;
        $menu->caption=$request->caption;
        $menu->links=$request->links;
        $menu->keywords=$request->keywords;
        $menu->metaDesc=$request->metaDesc;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        $menu->save();
        return redirect('menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Menu::find($id);

        $data->delete();
        return redirect('menu');
    }
}
