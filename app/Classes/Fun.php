<?php
namespace App\Classes;
use App\Setting;
use App\Group;
use App\Post;
use DB;

class Fun{
	public function group($id){
		//$setting=Setting::all();
		// $group=DB::table('posts')
  //           ->join('groups', 'posts.gid', '=', 'groups.id')->distinct()->select('posts.gid','groups.group')->where('posts.status','=',1)->get();
		$post=DB::table('posts')
            ->join('groups', 'posts.gid', '=', 'groups.id')->select('posts.*')->where([['posts.status','=',1],['posts.gid','=',$id]])->orderBy('created_at', 'desc')->limit(2)->get();
        return $post;
       //return view('front.home',['s_rows'=>$setting,'g_rows'=>$group]);
	}

	public function addvert_right($id){
		$add=DB::table('addverts')
            ->join('groups', 'addverts.gid', '=', 'groups.id')->select('addverts.*')->where([['addverts.status','=',1],['addverts.gid','=',$id],['addverts.type','=','right']])->orderBy('created_at', 'desc')->limit(2)->get();
        return $add;
       //return view('front.home',['s_rows'=>$setting,'g_rows'=>$group]);
	}

	public function addvert_down($id){
		$add=DB::table('addverts')
            ->join('groups', 'addverts.gid', '=', 'groups.id')->select('addverts.*')->where([['addverts.status','=',1],['addverts.gid','=',$id],['addverts.type','=','down'],['addverts.header','=',0]])->orderBy('created_at', 'desc')->limit(2)->get();
        return $add;
       //return view('front.home',['s_rows'=>$setting,'g_rows'=>$group]);
	}
}
?>