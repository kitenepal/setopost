<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('/', 'HomeController@index')->name('home');

// Admin Section *****
Route::get('/admin', 'AdminController@index')->name('home');
Route::get('/admin','AdminController@index');
Route::get('/addPost','AdminController@addPost');
Route::get('/group','AdminController@group');
Route::get('/menu','AdminController@menu');
Route::get('/setting','AdminController@setting');
Route::get('/post','AdminController@post');
Route::get('/addpost','AdminController@addpost');
Route::get('/addvert','AdminController@addvert');
Route::get('/title','AdminController@title');
Route::get('/slide','AdminController@slide');

Route::get('/multimedia','AdminController@multimedia');
Route::get('/slider','AdminController@slider');
Route::get('/user','AdminController@user');
Route::get('/login','HomeController@login')->name('login');
Route::get('/logout','HomeController@logout')->name('logout');

Auth::routes();

//Group Data
Route::post('/group','GroupController@store')->name('group.store');
Route::get('/group/edit/{id}','GroupController@edit')->name('group.edit');
Route::post('/group/update/{id}','GroupController@update')->name('group.update');
Route::get('/group/delete/{id}','GroupController@destroy')->name('group.destroy');

//Menu Data
Route::post('/menu','MenuController@store')->name('menu.store');
Route::get('/menu/edit/{id}','MenuController@edit')->name('menu.edit');
Route::post('/menu/update/{id}','MenuController@update')->name('menu.update');
Route::get('/menu/delete/{id}','MenuController@destroy')->name('menu.destroy');

//Setting Data
Route::get('/setting/edit/{id}','SettingController@edit')->name('setting.edit');
Route::post('/setting/update/{id}','SettingController@update')->name('setting.update');

//Post Data
Route::post('/post','PostController@store')->name('post.store');
Route::get('/post/edit/{id}','PostController@edit')->name('post.edit');
Route::post('/post/update/{id}','PostController@update')->name('post.update');
Route::get('/post/delete/{id}','PostController@destroy')->name('post.destroy');
Route::get('/post/view/{id}','PostController@show')->name('post.show');
Route::get('/post/activate/{id}','PostController@inactivate')->name('post.inactivate');
Route::get('/post/inactivate/{id}','PostController@activate')->name('post.activate');

//Addvert Data
Route::post('/addvert','AddvertController@store')->name('addvert.store');
Route::get('/addvert/edit/{id}','AddvertController@edit')->name('addvert.edit');
Route::post('/addvert/update/{id}','AddvertController@update')->name('addvert.update');
Route::get('/addvert/delete/{id}','AddvertController@destroy')->name('addvert.destroy');
Route::get('/addvert/activate/{id}','AddvertController@inactivate')->name('post.inactivate');
Route::get('/addvert/inactivate/{id}','AddvertController@activate')->name('post.activate');
Route::get('/addvert/header/{id}','AddvertController@header')->name('addvert.header');

//Title Data
Route::get('/title/store/{id}','TitleController@store')->name('title.store');
Route::get('/title/delete/{id}','TitleController@destroy')->name('title.destroy');
Route::get('/title/activate/{id}','TitleController@inactivate')->name('title.inactivate');
Route::get('/title/inactivate/{id}','TitleController@activate')->name('title.activate');

//Slide Data
Route::get('/slide/store/{id}','SlideController@store')->name('slide.store');
Route::get('/slide/delete/{id}','SlideController@destroy')->name('slide.destroy');
Route::get('/slide/activate/{id}','SlideController@inactivate')->name('slide.inactivate');
Route::get('/slide/inactivate/{id}','SlideController@activate')->name('slide.activate');

//Recent Data
Route::get('/recent/store/{id}','RecentController@store')->name('recent.store');
Route::get('/recent/delete/{id}','RecentController@destroy')->name('recent.destroy');
Route::get('/recent/activate/{id}','RecentController@inactivate')->name('recent.inactivate');
Route::get('/recent/inactivate/{id}','RecentController@activate')->name('recent.activate');

//Pages load
Route::get('/pages/{id}','HomeController@pages');
Route::get('/detail','HomeController@detail');