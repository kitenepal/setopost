<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addverts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->integer('header')->nullable();
            $table->integer('mid')->nullable();
            $table->integer('gid')->nullable();
            $table->string('type')->nullable();
            $table->date('from')->nullable();
            $table->date('till')->nullable();
             $table->string('meta')->nullable();
            $table->string('caption')->nullable();
            $table->string('links')->nullable();
            $table->string('keywords')->nullable();
            $table->string('metadesc')->nullable();
            $table->integer('status')->nullable();
            $table->integer('rank')->nullable();
            $table->integer('varifyby')->nullable();
            $table->date('varifydate')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addverts');
    }
}
