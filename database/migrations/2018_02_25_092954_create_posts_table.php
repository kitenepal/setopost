<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('feature_img')->nullable();
            $table->integer('gid');
            $table->integer('mid');
            $table->integer('post_by')->nullable();
            $table->integer('varify_by')->nullable();
            $table->string('varify_date')->nullable();
            $table->string('hits')->nullable();
            $table->integer('recent')->nullable();
            $table->string('meta')->nullable();
            $table->string('caption')->nullable();
            $table->string('links')->nullable();
            $table->string('keywords')->nullable();
            $table->integer('index')->nullable();
            $table->integer('status')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
