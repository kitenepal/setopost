@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Setting</li>
                  </div>
                </div>
              </ol>
            </div>
        </div>

       @foreach($rows as $data)
        <div class="row">
           
          <div class="col-lg-8" style="float: left;">
            <form action="{{url('/setting/update')}}/{{$result->id}}" method="post">
                {{csrf_field()}}
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="text">Title:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="title" value="{{$result->title}}">
                <br>


                <label for="text">Address:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="address" value="{{$result->address}}">
                <br>

                <label for="text">Contact:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="contact" value="{{$result->contact}}">
                <br>

                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email" value="{{$result->email}}">
                <br>

                <label for="text">Google Map:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="google_map" value="{{$result->google_map}}">
                <br>

                <label for="text">Facebook Link:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="facebook" value="{{$result->facebook}}">
                <br>  

                <label for="text">Twitter Link:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="twitter" value="{{$result->twitter}}">
                <br>

                <label for="text">Google Plus Link:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="google_plus" value="{{$result->google_plus}}">
                <br>

                <label for="text">Pin Interest Link:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="pininterest" value="{{$result->pinterest}}">
                <br>
                
                <label for="text">LinkedIn:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="linkedin" value="{{$result->linkedin}}">
                <br>

                <label for="text">Youtube Link:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="youtube" value="{{$result->youtube}}">
                <br>

                <label for="comment">Description:</label>
                  <textarea name="description" class="ckeditor">{{$result->desc}}</textarea>
              </div>
            </div>
          </div>
          

          <div class="col-lg-4">
              <div class="form-wrapper well">

                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="text">Meta Tag:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="meta" value="{{$result->meta}}">
                  <br>

                  <label for="text">Caption:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news" name="caption" value="{{$result->caption}}">
                  <br>

                  <label for="text">Black Links:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="links" value="{{$result->links}}">
                  <br>

                  <label for="text">Keywords:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news in nepal" name="keywords" value="{{$result->keywords}}">
                  <br>

                  <label for="text">Meta Description:</label>
                  <textarea cols="40" name="metaDesc">{{$result->metadesc}}</textarea>
                  <br>    

                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <label for="text">Logo:</label>
                    <img src="{{url('img')}}/{{$result->image}}" width="230px" height="80px">
                    <input type="file" class="form-control" id="file" name="file">
                    <input type="hidden" 
                </div>

                <div class="form-wrapper well">
                <div class="form-group">
          
                    <a href="{{url('group')}}"><div class="btn btn-primary"><b>Group Management</b></div></a>
                    <br><br>
                    <a href="{{url('menu')}}"><div class="btn btn-primary"><b>Menu Management</b></div></a>
                </div> 

                <div class="row">
                

              </div>     
              </div>

            </div>

            </div>
            <div class="col-lg-12">
                  <button class="btn btn-primary"><b>Save</b></button>
                  <!-- <button class="btn btn-default">Cancel</button>  -->
                </div>  
          </div>
        </div>
       @endforeach 

        </div>
        </form>
      </section>
    </section>
<!--main content end-->  
@endsection