@extends('back.layout.master')
@section('content')
 <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i>Home</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a></li>
              <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('admin')}}">
              <div class="info-box blue-bg">
                <i class="fa fa-home"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Home </h4>
              </div>
            <a href="slider.html">
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('post')}}">
              <div class="info-box brown-bg">
                <i class="fa fa-files-o" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Post </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('addvert')}}">
              <div class="info-box dark-bg">
                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Addvert </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('title')}}">
              <div class="info-box green-bg">
                <i class="fa fa-film" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Title </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
        </div>
        <div class="row">
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('slide')}}">
              <div class="info-box dark-bg">
                <i class="fa fa-sliders" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Slide</h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('activity')}}">
              <div class="info-box green-bg">
                <i class="fa fa-bell" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Activities</h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('user')}}">
              <div class="info-box brown-bg">
                <i class="fa fa-user" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Users </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
          <!--/.col-->
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a href="{{url('setting/edit/1')}}">
              <div class="info-box dark-bg">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <h4 style="margin:45px 0px;margin-top: 35px;font-weight: bold;float: left;"> Settings </h4>
              </div>
            </a>
            <!--/.info-box-->
          </div>
        </div>
      </section>
    </section>
    <!-- container section start -->
  
@endsection