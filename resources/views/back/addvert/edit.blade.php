@extends('back.addvert.index')
@section('addvert_view')
<div class="col-lg-4">
               <form action="{{url('/addvert/update')}}/{{$result->id}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
              <div class="form-wrapper well">
                <div class="form-group">
                  <label for="file">File:</label>
                  <img src="{{url($result->image)}}" width="240px" height="120px">
                  <input type="hidden" name="pic" value="{{$result->image}}">
                  <input type="file" class="form-control" id="text" name="image">
                  <br>

                  <!-- <div class="btn-group">
                  <label class="checkbox-inline">
                    @foreach($h_rows as $data)
                    <input type="checkbox" name="header" @if($data->header==1) checked @endif> Header</label>
                    @endforeach
                  </div>
                  <br><br> -->

                  <label for="sel1">Page:</label>
                  <select class="form-control" id="sel1" name="page">
                    @foreach($m_rows as $mdata)
                    <option value="{{$mdata->id}}" @if($result->mid==$mdata->id) selected @endif>{{$mdata->menu}}</option>
                    @endforeach
                  </select>
                  <br>

                  <label for="sel1">Section:</label>
                  <select class="form-control" id="sel1" name="section">
                  @foreach($g_rows as $gdata)
                  <option value="{{$gdata->id}}" @if($result->gid==$gdata->id) selected @endif>{{$gdata->group}}</option>
                  @endforeach
                  </select>
                  <br>

                  <label for="sel1">Type:</label>
                  <select class="form-control" id="sel1" name="type">
                    <option value="down" @if($result->type=='down') selected @endif>Down</option>
                    <option value="right" @if($result->type=='right') selected @endif>Right Side</option>
                  </select>
                  <br>

                  <label for="text">From:</label>
                  <input type="date" class="form-control" id="text" name="from" value="{{$result->from}}">
                  <br>

                  <label for="text">Till:</label>
                  <input type="date" class="form-control" id="text" name="till" value="{{$result->till}}">
                  <br>

                </div>
              </div>
            

              <div class="form-wrapper well">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="text">Meta Tag:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="meta" value="{{$result->meta}}">
                  <br>

                  <label for="text">Caption:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news" name="caption" value="{{$result->caption}}">
                  <br>

                  <label for="text">Black Links:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="links" value="{{$result->links}}">
                  <br>

                  <label for="text">Keywords:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news in nepal" name="keywords" value="{{$result->keywords}}">
                  <br>

                  <label for="text">Meta Description:</label>
                  <textarea cols="40" name="metaDesc">{{$result->metadesc}}</textarea>
                  <br>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <button class="btn btn-primary"><b>Save</b></button>
                    <!-- <button class="btn btn-default">Cancel</button> --> 
                  </div>
                </div>
              </div>     
              </div>
              </form>
            </div>
@endsection