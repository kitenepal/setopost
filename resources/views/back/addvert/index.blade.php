@extends('back.layout.master')
@section('content')
<!--main content start-->
<section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Advertisement</li>
                  </div>
                  <div class="col-md-6">
                    <li class="text-right"><a href="{{url('addvert')}}"><i class="fa fa-eye"></i>View</a></li>
                  </div>

                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <div class="form-wrapper well">
              <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>File</th>
                    <th>Page</th>
                    <th>Section</th>
                    <th>From</th>
                    <th>Till</th>
                    <th colspan="2" style="text-align: center;">Setting</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rows as $data)
                  <tr style="@if($data->header==1)background-color: darkorange @endif">
                    <th scope="row">{{$loop->iteration}}</th>
                    <td><img src="{{url($data->image)}}" width="100px" height="80px"></td>
                    <td>{{$data->menu}}</td>
                    <td>{{$data->group}}</td>
                    <td>{{$data->from}}</td>
                    <td>{{$data->till}}</td>
                    <td style="text-align: center;"><a href="{{url('/addvert/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{url('/addvert/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                    <td>
                    @if($data->status==1)
                      <span style="color:green;">Online</span>
                      @else
                      <span style="color:red;">Offline</span>
                      @endif
                      @if($data->status==1)
                      <a href="{{url('addvert/inactivate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:red;">Inactivate</div></a>
                      @else
                      <a href="{{url('addvert/activate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:lightgreen;">Activate</div></a>
                      @endif

                      <a href="{{url('/addvert/header')}}/{{$data->id}}">
                        <div class="btn btn-default" style="background-color:grey;">Header</div></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
            </div>
          </div>
             @yield('addvert_view')
          
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection