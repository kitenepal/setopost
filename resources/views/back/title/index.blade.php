@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Title News</li>
                </div>
                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('addpost')}}"><i class="fa fa-plus"></i>Add</a></li>
                </div>
              </div>
            </ol>
          </div>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th colspan="2" style="text-align: center;">Setting</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($t_rows as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->title_date}}</td>
                    <td>{{$data->title}}</td>
                    <td>
                      @if($data->status==1)
                      <span style="color:green;">Active</span>
                      @else
                      <span style="color:red;">Inactive</span>
                      @endif
                    </td>
                    <td style="text-align: center;"><a href="{{url('/post/view')}}/{{$data->pid}}"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                    <td style="text-align: center;"><a href="{{url('/title/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                    <td>
                       @if($data->status==1)
                      <a href="{{url('title/inactivate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:red;">Inactivate</div></a>
                      @else
                      <a href="{{url('title/activate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:lightgreen;">Activate</div></a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
          </div>

          <!-- Add Title News -->
          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-6">
                      <li class="text-left">Recent Posts</li>
                    </div>
                  </div>
                </ol>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Post Title</th>
                    <th>Posted Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($p_rows as $pst)
                  <tr>
                    <th scope="row">1</th>
                    <td>{{$pst->title}}</td>
                    <td>{{$pst->created_at}}</td>
                    <td>
                      
                      <a href="{{url('title/store')}}/{{$pst->id}}">
                      <div class="btn btn-default" style="background-color:blue;color:white;font-weight:bold; ">Select</div></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
          </div>
        </div>
       
      </section>
    </section>
    <!-- container section start -->
  @endsection