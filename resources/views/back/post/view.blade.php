@extends('back.layout.master')
@section('content')
<section id="main-content">
      <section class="wrapper">
        @foreach($rows as $data) 
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
                <div class="col-md-6">
                  <li class="text-left"><a href="{{url('post')}}"><i class="fa fa-eye"></i>View</a></li>
                </div>
                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('/post/edit')}}/{{$data->id}}"><i class="fa fa-edit"></i>Edit</a></li>
                </div>
            </ol>
            </div>
        </div>
        <div class="text-right">
          @if($data->status==1)
            <div class="btn btn-default" style="background-color:lightgreen;">online</div>
            @else
            <div class="btn btn-default" style="background-color:red;">offline</div>
          @endif
        </div>
          <div class="row">
            <div class="col-md-12">
              <h1 class="h-txt text-center">{{$data->title}}</h1>
              <center>
                <img src="{{url($data->feature_img)}}" width="20%" class="img-thumbnail">
             <!--  <img src="{{url('img/post')}}/{{$data->feature_img}}" width="20%" class="img-thumbnail"> -->
              </center>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              {{strip_tags($data->description)}}
            </div>
          </div>          

          <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
                <div class="col-md-6">
                  <li class="text-left"><a href="{{url('post')}}"><i class="fa fa-eye"></i>View</a></li>
                </div>
                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('/post/edit')}}/{{$data->id}}"><i class="fa fa-edit"></i>Edit</a></li>
                </div>
            </ol>
            </div>
        </div>
        @endforeach
        </section>
    </section>
@endsection