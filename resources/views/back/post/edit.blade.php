@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Edit Post</li>
                  </div>
                  <div class="col-md-6">
                    <li class="text-right"><a href="{{url('post')}}"><i class="fa fa-eye"></i>View</a></li>
                  </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            @foreach($rows as $data)
            <form action="{{url('/post/update')}}/{{$data->id}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="text">Title:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="title" value="{{$data->title}}">
                <br>
                <label for="text">Description:</label>
                  <textarea name="description" class="ckeditor">{{$data->description}}</textarea>

                <br>
                <label for="text">Group:</label>
                <select class="form-control" id="sel1" name="group">
                  @foreach($grows as $gdata)
                  <option value="{{$gdata->id}}" @if($data->gid==$gdata->id) selected @endif>{{$gdata->group}}</option>
                  @endforeach
                </select>
                
                <br>
                <label for="sel1">Category Menu:</label>
                <select class="form-control" id="sel1" name="menu">
                  @foreach($mrows as $mdata)
                  <option value="{{$mdata->id}}" @if($data->mid==$mdata->id) selected @endif>{{$mdata->menu}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
              <div class="form-wrapper well">

                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="text">Meta Tag:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="meta" value="{{$data->meta}}">
                  <br>

                  <label for="text">Caption:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news" name="caption" value="{{$data->caption}}">
                  <br>

                  <label for="text">Black Links:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="links" value="{{$data->links}}">
                  <br>

                  <label for="text">Keywords:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news in nepal" name="keywords" value="{{$data->keywords}}">
                  <br>
                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <label for="text">Feature Image:</label>
                    <img src="{{url($data->feature_img)}}" width="230px" height="150px">
                    <input type="file" class="form-control" id="file" name="image">
                    <input type="hidden" name="pic" value="{{$data->feature_img}}">
                    <br>
                </div>   
                
                <label for="text">Date:</label>
                  <input type="date" class="form-control" id="text" name="date" value="{{$data->created_at}}">

                @endforeach  
                
              </div>   
              </div>
              <br>
              <div class="row">
                <div class="col-lg-12">
                  <button class="btn btn-primary"><b>Update</b></button>
                  <!-- <button class="btn btn-default">Cancel</button>  -->
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection