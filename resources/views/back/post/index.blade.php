@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Post</li>
                </div>
                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('addpost')}}"><i class="fa fa-plus"></i>Add</a></li>
                </div>
              </div>
            </ol>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Post</th>
                    <th>Menu</th>
                    <th>Group</th>
                    <th>Status</th>
                    <th colspan="3" style="text-align: center;">Setting</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rows as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->title}}</td>
                    <td>{{$data->menu}}</td>
                    <td>{{$data->group}}</td>
                    <td>
                      @if($data->status==1)
                      <span style="color:green;">Active</span>
                      @else
                      <span style="color:red;">Inactive</span>
                      @endif
                    </td>
                    <td style="text-align: center;"><a href="{{url('/post/view')}}/{{$data->id}}"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                    <td style="text-align: center;"><a href="{{url('/post/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{url('/post/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                    <td>
                       @if($data->status==1)
                      <a href="{{url('post/inactivate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:red;">Inactivate</div></a>
                      @else
                      <a href="{{url('post/activate')}}/{{$data->id}}">
                      <div class="btn btn-default" style="background-color:lightgreen;">Activate</div></a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
          </div>
        </div>
       <!--  <button class="btn btn-default">Publish</button>
        <button class="btn btn-default">Cancel</button> -->
      </section>
    </section>
    <!-- container section start -->
  @endsection