@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Add Post</li>
                  </div>
                  <div class="col-md-6">
                    <li class="text-right"><a href="{{url('post')}}"><i class="fa fa-eye"></i>View</a></li>
                  </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <form action="{{url('/post')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="text">Title:</label>
                <input type="text" class="form-control" id="text" placeholder="Title goes here..." name="title">
                <br>
                <label for="text">Description:</label>
                  <textarea name="description" class="ckeditor"></textarea>

                <br>
                <label for="text">Group:</label>
                <select class="form-control" id="sel1" name="group">
                  @foreach($g_rows as $data)
                  <option value="{{$data->id}}">{{$data->group}}</option>
                  @endforeach
                </select>
                
                <br>
                <label for="sel1">Category Menu:</label>
                <select class="form-control" id="sel1" name="menu">
                  @foreach($m_rows as $data)
                  <option value="{{$data->id}}">{{$data->menu}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
              <div class="form-wrapper well">

                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="text">Meta Tag:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="meta">
                  <br>

                  <label for="text">Caption:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news" name="caption">
                  <br>

                  <label for="text">Black Links:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="links">
                  <br>

                  <label for="text">Keywords:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news in nepal" name="keywords">
                  <br>
                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <label for="text">Feature Image:</label>
                    <input type="file" class="form-control"  name="image">
                    <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
                    <br>
                </div>  

                <label for="text">Date:</label>
                  <input type="date" class="form-control" id="text" name="date">

              </div>     
              </div>
              <br>
              <div class="row">
                <div class="col-lg-12">
                  <button class="btn btn-primary"><b>Save</b></button>
                  <!-- <button class="btn btn-default">Cancel</button>  -->
                </div>
              </div>

            </div>

          </div>
        </div>

        
      </form>
      </section>
    </section>
<!--main content end-->  
@endsection