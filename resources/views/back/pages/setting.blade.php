@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Setting</li>
                  </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="email">Title:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Address:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Contact:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Google Map:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Facebook Link:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>  

                <label for="email">Twitter Link:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Google Plus Link:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>
                
                <label for="email">IndLink:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Pin Interest Link:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="email">Youtube Link:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>

                <label for="comment">Description:</label>
                  <textarea name="description" class="ckeditor"></textarea>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
              <div class="form-wrapper well">

                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="email">Meta Tag:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: latest news" name="email">
                  <br>

                  <label for="email">Caption:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news" name="email">
                  <br>

                  <label for="email">Black Links:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="email">
                  <br>

                  <label for="email">Keywords:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news in nepal" name="email">
                  <br>

                  <label for="email">Meta Description:</label>
                  <textarea cols="40"></textarea>
                  <br>

                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <label for="email">Logo:</label>
                    <input type="file" class="form-control" id="email" name="email">
                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <a href="{{url('group')}}"><button class="btn btn-primary"><b>Group Management</b></button></a>
                    <br><br>
                    <a href="{{url('menu')}}"><button class="btn btn-primary"><b>Menu Management</b></button></a>
                </div>   

              </div>     
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <button class="btn btn-primary"><b>Save</b></button>
            <button class="btn btn-default">Cancel</button> 
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection