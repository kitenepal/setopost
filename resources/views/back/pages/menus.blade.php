@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Menu</li>
                  </div>

                  <div class="col-md-6">
                  <li class="text-right"><a href="{{url('setting')}}"><i class="fa fa-cog"></i>Setting</a></li>
                </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <div class="form-wrapper well">
              <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Page Title</th>
                    <th>Description</th>
                    <th colspan="3" style="text-align: center;">Setting</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Home</td>
                    <td>Main page of your company.</td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>

                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Index</a></td>

                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>About</td>
                    <td>About page of your company.</td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>

                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Index</a></td>

                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Service</td>
                    <td>Service page of your company.</td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>

                    <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Index</a></td>

                  </tr>
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
            </div>
          </div>

          <div class="col-lg-4">
              <div class="form-wrapper well">
                <div class="form-group">
                  <label for="email">Menu:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: latest news" name="email">
                </div>
              </div>

              <div class="form-wrapper well">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="email">Meta Tag:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: latest news" name="email">
                  <br>

                  <label for="email">Caption:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news" name="email">
                  <br>

                  <label for="email">Black Links:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="email">
                  <br>

                  <label for="email">Keywords:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news in nepal" name="email">
                  <br>

                  <label for="email">Meta Description:</label>
                  <textarea cols="40"></textarea>
                  <br>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <button class="btn btn-primary"><b>Save</b></button>
                    <button class="btn btn-default">Cancel</button> 
                  </div>
                </div>
              </div>     
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection