@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Form</li>
                  </div>
                  <div class="col-md-6">
                    <li class="text-right"><a href="{{url('page')}}"><i class="fa fa-eye"></i>View</a></li>
                  </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="email">Title:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>
                <label for="comment">Description:</label>
                  <textarea name="description" class="ckeditor"></textarea>

                <br>
                <label for="sel1">Group:</label>
                <select class="form-control" id="sel1">
                  <option>Samachar</option>
                </select>
                
                <br>
                <label for="sel1">Category Menu:</label>
                <select class="form-control" id="sel1">
                  <option>About</option>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
              <div class="form-wrapper well">

                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="email">Meta Tag:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: latest news" name="email">
                  <br>

                  <label for="email">Caption:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news" name="email">
                  <br>

                  <label for="email">Black Links:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="email">
                  <br>

                  <label for="email">Keywords:</label>
                  <input type="email" class="form-control" id="email" placeholder="eg: education news in nepal" name="email">
                  <br>
                </div>

                <div class="form-wrapper well">
                <div class="form-group">
                    <label for="email">Feature Image:</label>
                    <input type="file" class="form-control" id="email" name="email">
                    <br>
                </div>   

              </div>     
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <button class="btn btn-primary"><b>Save</b></button>
            <button class="btn btn-default">Cancel</button> 
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection