@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
              <ol class="breadcrumb">
                <div class="row">
                  <div class="col-md-6">
                    <li><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Menu</li>
                  </div>

                  <div class="col-md-6">
                  <li class="text-right"><a href="{{url('setting/edit/1')}}"><i class="fa fa-cog"></i>Setting</a></li>
                </div>
                </div>
              </ol>
            </div>
        </div>

        <div class="row">
          <div class="col-lg-8" style="float: left;">
            <div class="form-wrapper well">
              <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Menu</th>
                    <th colspan="3" style="text-align: center;">Setting</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($rows as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->menu}}</td>
                    
                    <td style="text-align: center;"><a href="{{url('/menu/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>

                    <td style="text-align: center;"><a href="{{url('/menu/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                <ul class="pagination">
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </center>
            </div>
          </div>
            @yield('menu_view')          
          </div>
        </div>
      </section>
    </section>
<!--main content end-->  
@endsection