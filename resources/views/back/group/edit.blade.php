@extends('back.group.index')
@section('group_view')
<div class="col-lg-4">
              <form action="{{url('/group/update')}}/{{$result->id}}" method="post">
                {{csrf_field()}}
              <div class="form-wrapper well">
                <div class="form-group">
                  <label for="text">Group:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="group" value="{{$result->group}}">
                </div>
              </div>

              <div class="form-wrapper well">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO | Management</li>
                    </div>
                  </div>
                </ol>

                <div class="form-group">
                  <label for="text">Meta Tag:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: latest news" name="meta" value="{{$result->meta}}">
                  <br>

                  <label for="text">Caption:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news" name="caption" value="{{$result->caption}}">
                  <br>

                  <label for="text">Black Links:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: http:\\www.kitenepal.com\?share=news" name="links" value="{{$result->links}}">
                  <br>

                  <label for="text">Keywords:</label>
                  <input type="text" class="form-control" id="text" placeholder="eg: education news in nepal" name="keywords" value="{{$result->keywords}}">
                  <br>

                  <label for="text">Meta Description:</label>
                  <textarea cols="40" name="metaDesc" value="{{$result->metaDesc}}"></textarea>
                  <br>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <button class="btn btn-primary"><b>Update</b></button>
                    <!-- <button class="btn btn-default">Cancel</button> --> 
                  </div>
                </div>
              </div>     
              </div>
            
            </form>
            </div>
@endsection