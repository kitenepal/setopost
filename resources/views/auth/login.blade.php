<link href="{{url('backend/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="{{url('backend/css/bootstrap-theme.css')}}" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{url('backend/css/elegant-icons-style.css')}}" rel="stylesheet" />
  <link href="{{url('backend/css/font-awesome.css')}}" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="{{url('backend/css/style.css')}}" rel="stylesheet">
  <link href="{{url('backend/css/style-responsive.css')}}" rel="stylesheet" />

  <link rel="stylesheet" href="{{url('backend/stylesheets/module/login.css')}}">
  <script src="{{url('backend/js/login.js')}}"></script>

<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Login</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="wrapper">
    <form method="POST" class="login" action="{{ route('login') }}">
                        @csrf
      <p class="title">Log in</p>
      <input type="text" name="email" placeholder="Email" autofocus/>
      <i class="fa fa-user"></i>
      <input type="password" name="password" placeholder="Password" />
      <i class="fa fa-key"></i>
      <!-- <a href="{{ route('password.request') }}">Forgot your password?</a> -->
      <button>
        <i class="spinner"></i>
        <span class="state">Log in</span>
      </button>
    </form>
    <footer><a target="blank" href="http://kitenepal.com/">KITE Nepal</a></footer>
    </p>
  </div>
@include('back.common.footer')
