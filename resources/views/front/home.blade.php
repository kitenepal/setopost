<?php
use App\Classes\Fun;
?>

@extends('front.layout.master')
@section('content')
  <div class="section_part" style="margin-top: 10px;">
    <!--Left advertisement news-->
    <div class="left">
        <div class="main_heading" style="padding-left:90px; padding-bottom: 4px;">
        <h2>अहिले रोक्का जग्गा माधव नेपाल सरकारको पालामा क्षेत्र विस्तार गर्दाको</h2>
        </div>
        <!--News Part-->
        <div class="news_part">
            <!--First News-->
            <div id="news_one" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_1.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <a href="{{url('detail')}}"><button class="btn_3">पुरा पढ्नुहोस्</button></a>
                    </div>
                </div>
            </div>
            <!--End of First News-->
            <!--Second News-->
            <div id='news_two' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_2.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of Second News-->
            <!--third News-->
            <div id='news_three' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_3.png')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of third News-->
            <!--fourth News-->
            <div id='news_four' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_4.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of fourth News-->
            <!--fifth News-->
            <div id='news_five' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_5.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of fifth News-->
            <!--sixth News-->
            <div id='news_six' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_6.JPG')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of sixth News-->
            <!--seventh News-->
            <div id='news_seven' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_7.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of seventh News-->
            <!--eighth News-->
            <div id='news_eight' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_8.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of eighth News-->
            <!--ninth News-->
            <div id='news_nine' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_9.jpeg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of ninth News-->
            <!--tenth News-->
            <div id='news_ten' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{url('front/image/news_10.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of tenth News-->

            <!--News Slider Start-->
            <div class="news_slider">
                <div  id="owl-carousel-second" class="owl-carousel owl-theme">
                    <!--FIrst slider-->
                    <div class="item">
                        <div class="news_one" style="background-color: red;">
                            <img src="{{url('front/image/news_1.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--FIrst slider-->
                    <!--Second slider-->
                    <div class="item">
                        <div class="news_two">
                            <img src="{{url('front/image/news_2.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--Second slider-->
                    <!--Third slider-->
                    <div class="item">
                        <div class="news_three">
                            <img src="{{url('front/image/news_3.png')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--Third slider-->
                    <!--fourth slider-->
                    <div class="item">
                        <div class="news_four">
                            <img src="{{url('front/image/news_4.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--fourth slider-->
                    <!--fifth slider-->
                    <div class="item">
                        <div class="news_five">
                            <img src="{{url('front/image/news_5.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--fifth slider-->
                    <!--sixth slider-->
                    <div class="item">
                        <div class="news_six">
                            <img src="{{url('front/image/news_6.JPG')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--sixth slider-->
                    <!--seventh slider-->
                    <div class="item">
                        <div class="news_seven">
                            <img src="{{url('front/image/news_7.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--seventh slider-->
                    <!--eighth slider-->
                    <div class="item">
                        <div class="news_eight">
                            <img src="{{url('front/image/news_8.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--eighth slider-->
                    <!--ninth slider-->
                    <div class="item">
                        <div class="news_nine">
                            <img src="{{url('front/image/news_9.jpeg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--End of ninth slider-->
                    <!--Ten slider-->
                    <div class="item">
                        <div class="news_ten">
                            <img src="{{url('front/image/news_10.jpg')}}" class="rounded" alt="" width="100%" height="110px">
                            <p style="padding-top: 3px;">अहिले रोक्का जग्गा माधव नेपाल  ....</p> 
                        </div>
                    </div>
                    <!--End of Ten slider-->
                </div>
            </div>
            <!--End of News Slider-->
        </div>
        <!--End of News Part-->
        <div class="left_advertisement">
            <img src="{{url('front/image/left_advertisement_2.gif')}}" alt="" height="100px" width="90%">
        </div>


        <div class="important_news">
            <div class="main_news">
                <h5 style="font-weight: bold;">मुख्य समाचार <a href="#"><span style="float:right; padding-right:10px; color: white;">सबै</span></a></h5>
            </div>
            <h1>राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h1>
            <div class="post__info">
                <i class="fab fa-lyft"></i>
                <span>चिरञ्जीवी पौडेल</span>
                <i class="far fa-clock"></i>
                <span>२ घन्टा अगाडि</span>
                <i class="far fa-comment-alt"></i>
                <span>1</span>
            </div>
            <div class="hasImg">
                <img src="{{url('front/image/news_5.jpg')}}" alt="" width="100%" height="550px">
            </div>
            <p>२२ असार, काठमाडौं । एक समयका क्रान्तिकारी कम्युनिष्ट नेता सीपी मैनालीको छवि र हैसियत अहिले निकै फेरिएको छ । उनले राष्ट्रवादी वामपन्थी नेताको छवि बनाएका छन् । तर, उनको राजनीतिक शक्ति भने निकै क्षीण भइसकेको छ । मैनाली नेकपा मालेका महासचिव हुन् । तर, उनको पार्टीले गत संसदीय चुनावमा थ्रेसहोल्ड कटाउन नसकेपछि राष्ट्रिय पार्टीको दर्जा […]</p>
        </div>
        <div class="important_left_advertisement">
            <img src="{{url('front/image/left_advertisement_2.gif')}}" alt="" height="100px" width="90%">
        </div>
            
            <div class="important">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <div class="main_news">
            <h5 style="font-weight: bold;">मुख्य समाचार <a href="#"><span style="float:right; padding-right:10px; color: white;">सबै</span></a></h5>
        </div>
        <div class="post_news">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-12">
                        <img src="{{url('front/image/news_6.jpg')}}" alt="" width="100%" height="400px">
                        <h3 style="text-align: center">राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h3>
                        <div class="post__info">
                            <i class="fab fa-lyft"></i>
                            <span>चिरञ्जीवी पौडेल</span>
                            <i class="far fa-clock"></i>
                            <span>२ घन्टा अगाडि</span>
                            <i class="far fa-comment-alt"></i>
                            <span>1</span>
                        </div>
                    </div>
                    <div class="col-lg-5 col-12">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <div class="important_news_image">
                                    <img src="{{url('front/image/news_8.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <!--First News Start
        <div class="main_news">
            <h5 style="font-weight: bold;">मुख्य समाचार</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_1.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_2.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने, 
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="image/left_advertisement_3.gif" alt="" height="100px" width="90%">
        </div>
        -->
        <!--Second News Start
        <div class="main_news">
            <h5 style="font-weight: bold;">राजनीतिक</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_3.png" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_4.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="image/left_advertisement_4.gif" alt="" height="100px" width="90%">
        </div>
        -->
        <!--End of Second News Start-->
        <!--Third News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">अर्थ</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="{{url('front/image/news_5.jpg')}}" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="{{url('front/image/news_6.JPG')}}" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="{{url('front/image/left_advertisement_5.gif')}}" alt="" height="100px" width="90%">
        </div>
        <!--End of Third News Start-->
        <!--Fourth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;"> खेलकुद</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="{{url('front/image/news_7.jpg')}}" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="{{url('front/image/news_8.jpg')}}" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="{{url('front/image/left_advertisement_6.gif')}}" alt="" height="100px" width="90%">
        </div>
        <!--End of Fourth News Start-->
        <!--Fifth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">मनोरञ्जन</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="{{url('front/image/news_9.jpeg')}}" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="{{url('front/image/news_10.jpg')}}" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="{{url('front/image/left_advertisement_7.gif')}}" alt="" height="100px" width="90%">
        </div>
        <!--End of Fifth News Start-->
        <!--Sixth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">अन्य</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="{{url('front/image/news_11.jpg')}}" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="{{url('front/image/news_12.jpg')}}" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="{{url('front/image/left_advertisement_9.gif')}}" alt="" height="100px" width="90%">
        </div>
        <!--End of Sixth News Start-->
    </div>
    <!--End of Left advertisement news-->
    <!--right advertisement-->
    <div class="right">
       @include('front.pages.sidebar')
    </div>
    <!--End of right advertisement-->
</div>
@endsection