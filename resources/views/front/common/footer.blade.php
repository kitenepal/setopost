<footer>
<div class="container-fluid">
    <div class="row footer">
        <div class="col-lg-1 col-3">
            <img src="{{url('front/image/logo.jpg')}}" alt="logo" height="210px" width="100px">
        </div>    
        <div class="col-lg-3 col-9" style="color: white;">
            <div class="main_footer_first">
                <h1>News Portal</h1>
                <h6>977-1-4780076, 977-1-4786489</h6>
                <h6>news@onlinekhabar.com</h6>
                <h6>प्रकाशक - धर्मराज भुसाल</h6>
                <h6>सम्पादक - अरुण बराल </h6>
                <h6>सुचना बिभाग दर्ता नं. २१४ / ०७३–७४</h6>
            </div>
        </div>
        <div class="col-lg-1 col-1 text-center" style="color: white;">
            <div class="main_footer">
                <h6 style="font-weight: bold; padding-bottom: 10px;">समाचार</h6>
                <div class="last" style="">
                    <a href="" style="color: white;">समाज</a>
                    <a href="" style="color: white;">खेलकुद़़</a>
                    <a href="" style="color: white;">जीवनशैली/</a>
                    <a href="" style="color: white;">स्वास्थ्य</a>
                    <a href="" style="color: white;">प्रवास&nbsp;</a>
                    <a href="" style="color: white;">अन्तराष्ट्रिय</a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-2"></div>
        <div class="col-lg-1 col-1" style="color: white;">
            <div class="main_footer">
                <h6 style="font-weight: bold; padding-bottom: 10px;">विजनेश</h6>
                <div class="last" style="">
                    <a href="" style="color: white;">बजार&nbsp;&nbsp;</a>
                    <a href="" style="color: white;">पर्यटन</a>
                    <a href="" style="color: white;">रोजगार&nbsp;&nbsp;</a>
                    <a href="" style="color: white;">बैँक/ वित्त</a>
                    <a href="" style="color: white;">अटो</a>
                    <a href="" style="color: white;">सूचना</a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-2"></div>
        <div class="col-lg-1 col-1" style="color: white;">
            <div class="main_footer">
                <h6 style="font-weight: bold; padding-bottom: 10px;">मनोरञ्जन</h6> 
                <div class="last" style="">
                    <a href="" style="color: white;">ब्लोअप</a>
                    <a href="" style="color: white;">गसिप</a>
                    <a href="" style="color: white;">बलिउड/</a>
                    <a href="" style="color: white;">हलिउड</a>
                    <a href="" style="color: white;">भिडियो</a>
                    <a href="" style="color: white;">ताजा समाचार</a>
                </div>
            </div>
        </div>
        <div class="col-lg-1 col-2"></div>
        <div class="col-lg-1 col-1" style="color: white;">
            <div class="main_footer">
                <h6 style="font-weight: bold; padding-bottom: 10px;">विविध</h6> 
                <div class="last" style="">
                    <a href="" style="color: white;">विचार /ब्लग</a>
                    <a href="" style="color: white;">साहित्य</a>
                    <a href="" style="color: white;">अन्तर्वार्ता</a>
                    <a href="" style="color: white;">रोचक विश्व</a>
                    <a href="" style="color: white;">राम्रो नेपाल</a>
                </div>
            </div>
        </div>
    </div>
</div>
<hr style="border: 1px solid red; margin-bottom:0; margin-top: 0px;">
<p class="lastfooter" style="margin-bottom:0px; padding:10px;">&copy; 2019 News Portal. All Rights Reserved | KITE Nepal Technosis Pvt. Ltd.</p>
</footer>
<script src="{{url('front/js/jquery.js')}}"></script>
<script src="{{url('front/js/owl.carousel.min.js')}}"></script>
<script src="{{url('front/js/main.js')}}"></script>