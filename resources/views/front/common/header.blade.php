    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nepali News</title>
    <link rel="icon" href="{{url('front/image/logo.jpg')}}">
    <link rel="stylesheet" href="{{url('front/css/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{url('front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('front/css/all.css')}}" >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="{{url('front/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{url('front/css/style.css')}}">
    <link rel="stylesheet" href="{{url('front/css/responsive.css')}}">