@extends('front.layout.master')
@section('content')

<div class="container">
        @foreach($mn_rows as $data)
        <div class="pageNavigation">
          <p><a href="{{url('')}}">मूलपाना</a> >> <a href="#">{{$data->menu}}</a></p>
        </div>
        @endforeach
      </div>
      <div class="news">
        <div class="box">
          <div class="container">
            @foreach($mn_rows as $data)
            <h2 class="h-txt">{{$data->menu}}</h2>
            @endforeach
            <hr class="style1">
            <div class="">
              <!-- Here was container -->
              <div class="row">
                <div class="col-md-8">

                  @foreach($p_rows as $data)
                  <div class="row box--wrap">
                    <div class="col-md-4 box--image">
                      <a href="{{url('detail')}}/{{$data->id}}/mid/{{$data->mid}}"><img src="{{url($data->feature_img)}}" alt="{{$data->title}}" width="262.5" height="164.267"></a>
                    </div>
                    <div class="col-md-8 box--news">
                      <a href="{{url('detail')}}/{{$data->id}}/mid/{{$data->mid}}"><h3 class="h-txt">{{$data->title}}</h3></a>
                      <p>
                        {{str_limit(strip_tags($data->description),400)}}... <a href="{{url('detail')}}/{{$data->id}}/mid/{{$data->mid}}" class="a-txt">थप जानकारी</a>
                        <iframe class="social-share" src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fwww.facebook.com%2Fnuwagimonthly&layout=button_count&size=small&mobile_iframe=true&width=83&height=20&appId" width="83" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                      </p>
                    </div>
                  </div>
                  @endforeach
                </div>

                <div class="col-md-3 box--ads">
                  <img src="{{url('front/img/nuwagiad.jpg')}}" alt="samachar1" width="247.5" height="auto">
                </div>
                
                <div class="col-md-8">
                  <ul class="pagination pagination-sm">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">>></a></li>
                  </ul>
                </div>
                <div class="col-md-8 box--bannerAdvertise">
                  <img src="https://www.nuwagi.com/wp-content/uploads/2017/04/Toyota-Ratopati_850x150.gif" alt="Advertise" width="100%">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection