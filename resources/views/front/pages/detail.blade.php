@extends('front.layout.master')
@section('content')
    <div class="row">
    <div class="left">
        <div class="left_advertisement">
            <img src="image/left_advertisement_7.gif" alt="" height="100px" width="100%">
        </div>
        <div class="back_title_part">
            <a href="#">होमपेज / </a>
            <a href="#">समाचार / </a>
            <a class="current_page" href="#">प्रमुख समाचार</a>
        </div>
        <h1>जनकपुरमा अस्पतालसमेत डुवानमा, सर्लाहीका १५० परिवारलाई सारियो</h1>
        <div class="left_advertisement">
            <img src="image/left_advertisement_3.gif" alt="" height="100px" width="100%">
        </div>
        <div class="samachar_news">
            <div class="samachar_img">
                <img src="image/news_6.jpg" alt="" width="100%">
            </div>
            <p>२७ असार, जनकपुरधाम । निरन्तरको बर्षाका कारण प्रदेश २ को अस्थायी राजधानी जनकपुरधाम डुबानमा परेको छ ।</p>
            <p>भानुचोक, शिवचोक लगायतका मुख्य चोक जलमग्न भएको छ भने घर–घरसम्म पानी पसेको छ । तीन दिनदेखि लगातार परेको वर्षका कारण जनकपुर चुरोट कारखानाभित्र रहेको मुख्यमन्त्रीको कार्यालय, मुरलीचोकस्थित संघीय प्रहरी कार्यालय डुबानमा परेको छ ।</p>
            <p>जनकपुर अञ्चल अस्पताल, जिल्ला प्रहरी कार्यालय, बाह्रबिघा मैदान, विमानस्थल, जिल्ला हुलाक कार्यालय, प्रदेश २ को स्वास्थ्य निर्देशनालय पनि डुबानमा परेको छ ।</p>
            <div class="samachar_img">
                    <img src="image/news_5.jpg" alt="" width="100%">
            </div>
            <p>जनकपुरधाम उपमहानगरपालिका कार्यालय अगाडिकै सडकमा पानी जमेको छ । प्रदेश प्रहरी कार्यालयका अनुसार जनकपुरधाम उपमहानगरपालिकाका २५ वटै वडामा १ देखि २ फिटसम्म पानी जमेको छ ।</p>
            <p>प्रदेश प्रहरी प्रवक्ता ज्ञानकुमार महतोले प्रदेश अन्तर्गतका मुख्य नदीहरुको जलस्तर बढेको बताए । कोशी, बलान, कमला, बकैया नदीको जलस्तर बढेको हो ।</p>
            <strong>बरहथवाका १५० परिवारलाई सारियो</strong>
            <p>सर्लाहीको बरहथवा नगरपालिका १५ स्थित गाउँ नै डुबानमा परेको छ । गाउँमा जमेको पानीको निकास नहुँदा स्थानीय १ सय ५० परिवारलाई नजिकैको विद्यालयमा सुरक्षित तरिकाले राखिएको छ ।</p>
            <strong>सप्तरीमा सडक अवरुद्व</strong>
            <p>सप्तरीको दक्षिणी क्षेत्रका गाउँहरुमा पानी पसे पनि खतराको अवस्था नरहेको प्रहरीले जनाएको छ । खाँडो नदीमा आएको बाढीका कारण राजविराज–कुनौली सडक खण्ड अन्तर्गत तिलाठी–कोइलाडी १ मा बाटो भत्किएको छ । बाटो कटान गरेका कारण सबारी आवागमन पूर्णरुपले ठप्प भएको छ । सप्तरीसहित खाँडोसहित खड्ग, सुन्दरी, बलानसहितका नदीहरुको जलस्तर बढेको छ ।</p>
            <strong>सिरहाका कार्यालयहरु डुबानमा</strong>
            <p>निरन्तरको बर्षाले सिरहा जिल्ला पनि प्रभावित भएको छ । सदरमुकाम सिरहासहित गोलबजार र लहान नगरपालिका पनि डुबानमा परेको छ । सिरहा नगरपालिकाका बजार क्षेत्रका साथै सरकारी कार्यालयहरु समेत डुबानमा परेको छ ।</p>
            <p>तत्कालीन जिल्ला कृषि विकास कार्यालय, जिल्ला पशु सेवा कार्यालय, नेपाल विद्युत प्राधिकरण कार्यालय, जिल्ला प्रहरी कार्यालयलगायतका सरकारी कार्यालय जलमग्न भएका छन् ।</p>
            <p>गोलबजारका केही क्षेत्र जलमग्न भएको छ भने पूर्वपश्चिम राजमार्गमै पानी जमेको छ । अविरल वर्षाका कारण इलाका प्रहरी कार्यालय लहानसमेत जलमग्न भएको छ । कार्यालय परिसरमा करीब तीन/चार फिटसम्म पानी जम्दा पीडितलाई कार्यालयसम्म पुग्न समस्या भइरहेको स्थानीयले बताएका छन् ।</p>
        </div>
        
        <div class="all_news_heading">
            <div class="news_heading">
                <h2>छुटाउनुभयो कि?</h2>
            </div>
            <div class="news_heading_all_details">
                <a href="#"> <h2>सबै </h2></a>
            </div>
        </div>


        <div class="All_News_Page" id="owl-carousel-first">
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
            <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
        </div>      
    </div>
    <div class="right">
        <button class="btn">थप समाचारहरु</button>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_1.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_2.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_3.png" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="margin-bottom: 10px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_1.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_2.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_3.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_4.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_5.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_6.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_4.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_5.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_6.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" >
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_7.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="owl_news_heading">
    <div class="news_heading">
        <h2>छुटाउनुभयो कि?</h2>
    </div>
    <div class="news_heading_all_details">
        <a href="#"> <h2>सबै </h2></a>
    </div>
</div>
<div id="owl_news" class="owl-carousel owl-theme" style="padding: 10px 20px">
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
    <div class="item">
        <div class="Show_news item">
                <div class="News_Img">
                    <a href="#">
                        <img src="image/news_6.jpg" alt="">
                    </a>
                </div>
                <div class="News_Text">
                    <a href=""> <p>विमला तामाङको मृत्युबारे छानविन गर्न समिति गठनको माग</p></a>
                </div>
            </div>
    </div>
</div>

@endsection